package com.cfhayes.algorithms.arrays;

import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Feature("Data Structures")
@DisplayName("Implementing a stack using a Linked List")
public class LinkedStackOfStringsTest {

    @Test
    public void testScenario1() {
        LinkedStackOfStrings stackOfStrings = new LinkedStackOfStrings();
        assertThat(stackOfStrings.isEmpty()).isTrue();        
    }
}
