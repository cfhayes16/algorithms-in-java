package com.cfhayes.algorithms.arrays;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class AppendAndDeleteTest {

    @ParameterizedTest(name = "Should be able to merge string {0} with string {1} with {2} moves")
    @MethodSource("dataProvider")
    public void testScenario1(String s1, String s2, int m, String expected) {
        String result = AppendAndDelete.appendAndDelete(s1, s2, m);
        assertThat(result).isEqualTo(expected);
    }

    static Stream<Arguments> dataProvider() {
        return Stream.of(
//                arguments("hackerhappy", "hackerrank", 9, "Yes"),
                arguments("aba", "aba", 7, "Yes"),
//                arguments("ashley", "ash", 2, "No"),
                arguments("bob", "bob", 1, "Yes")
//                arguments(" ", " ", 1, "Yes"),
//                arguments("a", "b", 2, "Yes"),
//                arguments("a", "b", 1, "No")
//                arguments("y", "yu", 2, "No")
        );
    }
}
