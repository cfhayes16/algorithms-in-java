package com.cfhayes.algorithms.arrays;

import com.cfhayes.algorithms.arrays.Duplicates;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DuplicatesTest {

    @Test
    @DisplayName("The input array has no duplicates")
    public void testScenario1() {
        Duplicates duplicates = new Duplicates();
        long start = System.nanoTime();
        boolean hasDuplicates = duplicates.containsDuplicates(new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13});
        long elapsed = System.nanoTime() - start;
        System.out.println("test case 1: " + elapsed + " nanoseconds");
        assertThat(hasDuplicates).isFalse();
    }

    @Test
    @DisplayName("The input array has one duplicate")
    public void testScenario2() {
        Duplicates duplicates = new Duplicates();
        long start = System.nanoTime();
        boolean hasDuplicates = duplicates.containsDuplicates(new int[]{1,2,3,4,5,6,7,8,9,10,11,13,13});
        long elapsed = System.nanoTime() - start;
        System.out.println("test case 2: " + elapsed + " nanoseconds");
        assertThat(hasDuplicates).isTrue();
    }

    @Test
    @DisplayName("The input array has no duplicates")
    public void testScenario3() {
        Duplicates duplicates = new Duplicates();
        long start = System.nanoTime();
        boolean hasDuplicates = duplicates.containsDuplicates(new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13});
        long elapsed = System.nanoTime() - start;
        System.out.println("test case 3: " + elapsed + " nanoseconds");
        assertThat(hasDuplicates).isFalse();
    }
}