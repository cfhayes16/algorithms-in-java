package com.cfhayes.algorithms.arrays;

import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Feature("Array Problems")
@DisplayName("Determine if a given array has 2 values whos sum matches a given value")
public class FindMatchingSumTest {

    @Test
    @DisplayName("Array with one pair of values whos sum equals the target")
    public void testScenario1() {
        FindMatchingSum app = new FindMatchingSum();
        int[] values = {1,3,6,7,8};
        int target = 7;
        assertThat(app.hasMatchingSum(values, target)).isTrue();
    }
}
