package com.cfhayes.algorithms.arrays;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class ResizingArrayStackOfStringsTest {

    @Test
    @DisplayName("Should extend array when full")
    void testScenario1() {
        ResizingArrayStackOfStrings app = new ResizingArrayStackOfStrings(3);
        assertThat(app.isEmpty()).isTrue();
        app.push("one");
        app.push("two");
        app.push("three");
        app.push("four");
        assertThat(app.getSize()).isEqualTo(6);
    }

}
