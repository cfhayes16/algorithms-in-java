package com.cfhayes.algorithms.arrays;

import com.cfhayes.algorithms.arrays.FixedCapacityStackOfStrings;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class FixedCapacityStackOfStringsTest {

    @Test
    @DisplayName("Should push and pop strings")
    void testScenario1() {
        FixedCapacityStackOfStrings app = new FixedCapacityStackOfStrings(3);
        assertThat(app.isEmpty()).isTrue();
        app.push("one");
        assertThat(app.isEmpty()).isFalse();
        assertThat(app.pop()).isEqualTo("one");
        assertThat(app.isEmpty()).isTrue();
    }

    @Test
    @DisplayName("Should push and pop null values")
    void testScenario2() {
        FixedCapacityStackOfStrings app = new FixedCapacityStackOfStrings(3);
        assertThat(app.isEmpty()).isTrue();
        app.push(null);
        assertThat(app.isEmpty()).isFalse();
        assertThat(app.pop()).isEqualTo(null);
        assertThat(app.isEmpty()).isTrue();
    }
}
