package com.cfhayes.algorithms.basic;

import com.cfhayes.algorithms.util.StopWatch;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class BubbleSortTest {

    @ParameterizedTest(name = "Should sort list {0} with {1} swaps")
    @MethodSource("dataProvider")
    void testSort(int[] a, int swaps) {
        StopWatch.start();
        BubbleSort.sort(a);
        StopWatch.stop();
    }

    static Stream<Arguments> dataProvider() {
        return Stream.of(
                Arguments.of(new int[]{6,2,5,1,22,101,7,12}, 2),
                Arguments.of(new int[]{3,4,5}, 0)
        );
    }
}
