package com.cfhayes.algorithms.basic;

import com.cfhayes.algorithms.util.StopWatch;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class QuickSortTest {

  @ParameterizedTest(name = "Should sort list {0}")
  @MethodSource("dataProvider")
  void testSort(int[] a) {
    StopWatch.start();
    QuickSort.sort(a);
    StopWatch.stop();
  }

  static Stream<Arguments> dataProvider() {
    return Stream.of(
      Arguments.of(new int[]{20,6,8,53,56,23,87,41,49,19})
    );
  }
}
