package com.cfhayes.ds;

import com.cfhayes.algorithms.util.Node;
import com.cfhayes.algorithms.util.StopWatch;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class DoubleLinkedListTest {

  @ParameterizedTest(name = "Input array {0}, find {1}")
  @MethodSource("findNodeDataProvider")
  void testFindNode(int[] values, int target) {
    LinkedList linkedList = new LinkedList();
    for (int value: values) {
      linkedList.insertAtHead(value);
    }
    StopWatch.start();
    Node<Integer> node = linkedList.findNode(target);
    StopWatch.stop();
    assertThat(node).isNotNull();
    assertThat(node.getValue()).isEqualTo(target);
  }

  @ParameterizedTest(name = "Input array {0}, find {1}")
  @MethodSource("findNodeNotFoundDataProvider")
  void testFindNodeNotFound(int[] values, int target) {
    LinkedList linkedList = new LinkedList();
    for (int value: values) {
      linkedList.insertAtHead(value);
    }
    StopWatch.start();
    Node<Integer> node = linkedList.findNode(target);
    StopWatch.stop();
    assertThat(node).isNull();
  }

  static Stream<Arguments> dataProvider() {
    return Stream.of(
      Arguments.of(new int[]{20,6,8}, 3),
      Arguments.of(new int[]{2,5,9,11,13,20}, 6)
    );
  }
  static Stream<Arguments> findNodeDataProvider() {
    return Stream.of(
            Arguments.of(new int[]{20,6,8}, 8),
            Arguments.of(new int[]{2,5,9,11,13,20}, 13)
    );
  }
  static Stream<Arguments> findNodeNotFoundDataProvider() {
    return Stream.of(
            Arguments.of(new int[]{}, 8),
            Arguments.of(new int[]{2,5,9,11,13,20}, 21)
    );
  }
}
