package com.cfhayes.ds;

import com.cfhayes.algorithms.util.StopWatch;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class ResizableArrayTest {

  @ParameterizedTest(name = "Adding items {0}. Expected size is {1}. New capacity is {2}.")
  @MethodSource("dataProvider")
  void testAddItems(String[] values, int expectedSize, int capacity) {
    StopWatch.start();
    ResizableArray arr = new ResizableArray();
    for (String value : values) {
      arr.push(value);
    }
    StopWatch.stop();
    assertThat(arr.getNumItems()).isEqualTo(expectedSize);
    assertThat(arr.getCapacity()).isEqualTo(capacity);
  }

  static Stream<Arguments> dataProvider() {
    return Stream.of(
      Arguments.of(new String[]{"one","two","three"}, 3, 4),
      Arguments.of(new String[]{"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"}, 52, 64)
    );
  }

}
