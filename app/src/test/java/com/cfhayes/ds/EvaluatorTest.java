package com.cfhayes.ds;

import com.cfhayes.algorithms.util.StopWatch;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class EvaluatorTest {

  @ParameterizedTest(name = "Expression {0} should evaluate to {1}")
  @MethodSource("dataProvider")
  void testAddNodes(String expression, double answer) {
    StopWatch.start();
    double result = Evaluator.evaluate(expression);
    StopWatch.stop();
    assertThat(result).isEqualTo(answer);
  }

  static Stream<Arguments> dataProvider() {
    return Stream.of(
      Arguments.of("(1+((2+3)*(4*5)))", 101),
      Arguments.of("(1+(2*3)*(2+2))", 24)
    );
  }
}
