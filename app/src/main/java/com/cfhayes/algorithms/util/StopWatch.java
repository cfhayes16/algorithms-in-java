package com.cfhayes.algorithms.util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class StopWatch {

    private static long startTime;
    private static long elapsedTime;

    private StopWatch(){}

    public static void start() {
        startTime = System.nanoTime();
    }

    public static void stop() {
        elapsedTime = System.nanoTime() - startTime;
        System.out.println("Execution time: " + elapsedTime + " ns" + " ("
                + TimeUnit.MILLISECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS) + " ms)");
    }
}
