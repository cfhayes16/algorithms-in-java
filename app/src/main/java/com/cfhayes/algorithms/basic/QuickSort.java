package com.cfhayes.algorithms.basic;

public class QuickSort {
  public static void sort(int[] array) {
    if (array.length <= 7) {
      for (int i = 1; i < array.length; i++)
        for (int j = i; j > 0 && array[j - 1] > array[j]; j--) {
          int temp = array[j - 1];
          array[j - 1] = array[j];
          array[j] = temp;
        }
        return;
    }
    int mid = array.length / 2;
    int lo = 0;
    int hi = array.length - 1;
  }
}
