package com.cfhayes.algorithms;

import java.util.List;

import java.util.ArrayList;

public class ClosestPlane {

    public static void main(String[] args) {

        ClosestPlane closestPlane = new ClosestPlane();

        List<Point> allPlanes = new ArrayList<>();

        allPlanes.add(new Point(5, 5));
        allPlanes.add(new Point(15, 66));
        allPlanes.add(new Point(105, 42));
        allPlanes.add(new Point(7, 6));
        System.out.println("Minimum distance: " + closestPlane.minimumDistance(allPlanes));

    }

    public double minimumDistance(List<Point> planes) {
        double minDistance = Double.MAX_VALUE;
        for (Point p1 : planes) {
            for (Point p2 : planes) {
                double distance = p1.distanceTo(p2);
                if (distance != 0 && distance < minDistance) minDistance = distance;
            }
        }
        return minDistance;
    }
}