package com.cfhayes.algorithms;

import java.math.BigInteger;

public class BigFactorial {

    static void extraLongFactorials(int n) {
        BigInteger result = BigInteger.ONE;
        for (int i = 2; i <= n; i++)
            result = result.multiply(BigInteger.valueOf(i));
        System.out.println(result);
    }

    public static void main(String[] args) {
        BigFactorial.extraLongFactorials(100);
    }
}
