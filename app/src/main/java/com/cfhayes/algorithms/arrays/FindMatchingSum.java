package com.cfhayes.algorithms.arrays;

import java.util.Set;
import java.util.HashSet;

public class FindMatchingSum {

    public boolean hasMatchingSum(int[] arr, int target) {
        Set<Integer> foundValues = new HashSet<Integer>();
        for (int v : arr) {
            if (foundValues.contains(target - v)) {
                return true;
            }
            foundValues.add(v);
        }
        return false;
    }
}
