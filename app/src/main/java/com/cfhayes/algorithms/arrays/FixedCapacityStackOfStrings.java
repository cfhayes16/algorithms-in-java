package com.cfhayes.algorithms.arrays;

/**
 * Takes a fixed size array that can hold strings and implements
 * a stack which allows us to push and pop vales to and from the
 * stack.
 */
public class FixedCapacityStackOfStrings {
    private String[] s;
    private int N = 0;

    public FixedCapacityStackOfStrings(int capacity) {
        s = new String[capacity];
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public void push(String item) {
        s[N++] = item;
    }

    public String pop() {
        return s[--N];
    }

}