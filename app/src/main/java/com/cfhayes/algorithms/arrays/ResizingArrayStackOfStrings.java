package com.cfhayes.algorithms.arrays;

/**
 * When an array is full, create another array twice the size
 * of the original and copy the items over to the new array.
 */
public class ResizingArrayStackOfStrings {
    private String[] s;
    private int N = 0;

    public ResizingArrayStackOfStrings(int capacity) {
        s = new String[capacity];
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public boolean isFull() {
        return N == s.length;
    }

    public void push(String item) {
        if (isFull()) {
            resize(2 * s.length);
        }
        s[N++] = item;
    }

    public String pop() {
        return s[--N];
    }

    public int getSize() {
        return s.length;
    }

    private void resize(int capacity) {
        String[] s2 = new String[capacity];
        System.arraycopy(s, 0, s2, 0, s.length);
        s = s2;
    }
}