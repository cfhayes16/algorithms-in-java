package com.cfhayes.algorithms.arrays;

public class AppendAndDelete {

    static String appendAndDelete(String s, String t, int k) {
        assert (s.length() >= 1 && s.length() <= 100);
        assert (t.length() >= 1 && t.length() <= 100);
        assert (k >= 1 && k <= 100);

        // If the difference in length between the 2 strings
        // is greater than k, then just return No
        if ((Math.abs(s.length() - t.length())) > k) {
            return "No";
        }

        // If both strings are equal, take no action and return Yes
        if (s.compareTo(t) == 0) {
            return "Yes";
        }

        int nonMatchIdx = -1;
        if (s.length() > t.length()) {
            nonMatchIdx = findNonMatchIndex(s, t);
        } else {
            nonMatchIdx = findNonMatchIndex(t, s);
        }

        String delStr = s.length() > 1 ? s.substring(nonMatchIdx, s.length()) : "";
        String addStr = t.substring(nonMatchIdx, t.length());

        if (delStr.length() + addStr.length() <= k) {
            return "Yes";
        }
        return "No";
    }

    static boolean isLowerAlphabet(char c) {
        return c >= 'a' && c <= 'z';
    }

    static int findNonMatchIndex(String s1, String s2) {
        int t = 0;
        int i;

        for (i = 0; i < s1.length(); i++) {
            if (t == s2.length())
                break;
            char s1Char = s1.charAt(i);
            char s2Char = s2.charAt(t);
            if (isLowerAlphabet(s1Char) && isLowerAlphabet(s2Char)) {
                if (s1Char == s2Char) {
                    t++;
                }
            } else {
                break;
            }
        }
        return t;
    }
}
