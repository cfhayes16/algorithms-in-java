package com.cfhayes.ds;

/**
 * A basic example of an array that can be resized. This is only
 * for educational purposes and is not intended for production use.
 */
public class ResizableArray {

    private String[] s;
    private int numItems = 0;
    private int capacity = 1;

    public ResizableArray() {
        s = new String[capacity];
    }

    public void push(String value) {
        if (numItems == s.length) {
            resize(2 * s.length);
        }
        s[numItems++] = value;
    }

    public int getNumItems() {
        return numItems;
    }

    public int getCapacity() {
        return capacity;
    }

    private void resize(int capacity) {
        String[] copy = new String[capacity];
        this.capacity = capacity;
        for (int i = 0; i < numItems; i++) {
            copy[i] = s[i];
        }
        s = copy;
    }
}
