package com.cfhayes.ds;

import com.cfhayes.algorithms.util.Node;

/**
 * An example of creating a linked list from scratch.
 */
public class LinkedList {
    private Node<Integer> head;
    private int length;

    /**
     * Creates a new node with the provided value and
     * inserts the new node at the head of the list.
     * @param value An integer value
     */
    public void insertAtHead(int value) {
        Node<Integer> node = new Node<>(value);
        node.setNext(this.head);
        this.head = node;
        length++;
    }

    /**
     * Deletes the node at the head of the list
     */
    public void deleteHeadNode() {
        this.head = this.head.getNext();
    }

    /**
     * Attempts to find a node with the given value.
     * @param data The value of the node to search for.
     * @return Null if no node was found, otherwise,
     * returns the found node.
     */
    public Node<Integer> findNode(int data) {
        Node<Integer> current = this.head;
        while (current != null) {
            if (current.getValue() == data) {
                return current;
            }
            current = current.getNext();
        }
        return null;
    }

    public int getLength() {
        return this.length;
    }
}
