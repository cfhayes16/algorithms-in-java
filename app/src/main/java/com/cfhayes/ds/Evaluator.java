package com.cfhayes.ds;

import java.util.Stack;

public class Evaluator {

    public static double evaluate(String exp) {
        Stack<String> ops = new Stack<>();
        Stack<Double> vals = new Stack<>();
        char[] chars = exp.toCharArray();

        for (char c : chars) {
            String s = String.valueOf(c);
            if (s.equals("("));
            else if (s.equals("+")) ops.push(s);
            else if (s.equals("*")) ops.push(s);
            else if (s.equals(")")) {
                String op = ops.pop();
                if (op.equals("+")) vals.push(vals.pop() + vals.pop());
                if (op.equals("*")) vals.push(vals.pop() * vals.pop());
            }
            else vals.push(Double.parseDouble(s));
        }
        return vals.pop();
    }
}
